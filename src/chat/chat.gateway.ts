import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer
} from '@nestjs/websockets';


@WebSocketGateway()
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer()
    server;

    usersCnt = 0;

    async handleConnection() {
        this.usersCnt += 1;

        this.server.emit('users-count', this.usersCnt);
    }

    async handleDisconnect() {
        this.usersCnt -= 1;

        this.server.emit('users-count', this.usersCnt);
    }

    @SubscribeMessage('chat')
    async onChat(client, message) {
        client.broadcast.emit('chat', message);
    }

}
